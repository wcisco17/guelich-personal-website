"use strict";
require("dotenv").config({
  path: `.env`
});

const path = require(`path`);

module.exports = {
  siteMetadata: {
    title: "ULRIKE GUELICH",
    siteTitle: "Dr. Ulrike Guelich / Speaker & Moderator",
    description: "Looking at Female Entrepreneurship in Thailand and Asean",
    author: `@wcisco17 | Williams Sissoko`
  },

  plugins: [
    {
      resolve: "gatsby-plugin-generate-types",
      options: {
        inProduction: true
      }
    },

    // Prismic Configurations here. --> NEEDS REVIEW
    {
      resolve: "gatsby-source-prismic",
      options: {
        repositoryName: process.env.PRISMIC_REPO_NAME, // (REQUIRED, replace with your own)
        accessToken: process.env.PRISMIC_VARIABLE,
        defaultLang: "en-us"
      }
    },

    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: path.join(__dirname, `src`, `images`)
      }
    },

    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Dr. Ulrike Guelich / Speaker & Moderator`,
        display: `minimal-ui`,
        path: `${__dirname}/src/images`,
        icon: `src/images/guelich-logo.png`
      }
    },

    `gatsby-plugin-styled-components`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-catch-links`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-netlify`,
    `gatsby-plugin-offline`
  ]
};
