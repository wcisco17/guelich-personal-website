import { useState } from "react";

export const FORM_ID = "a3e38388-f1e1-4fff-a852-b545b3802a9f";

export const useInput = (initialValue: string) => {
  const [value, setValue] = useState(initialValue);

  return {
    value,
    setValue,
    reset: () => setValue(""),
    bind: {
      value,
      onChange: (event: any) => {
        setValue(event.target.value);
      }
    }
  };
};

export const links = {
  ld:
    "https://th.linkedin.com/in/ulrike-guelich-02a4b514?trk=author_mini-profile_title",
  tweet: "https://twitter.com/uli_busem?lang=en",
  fac:
    "https://www.facebook.com/WLEConference/posts/we-are-proud-to-announce-asst-prof-dr-ulrike-guelich-as-keynote-speaker-of-9th-a/739167719615714/"
};
