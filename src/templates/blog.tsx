import { graphql } from 'gatsby';
import * as React from 'react';
import { Fragment } from 'react';
import { Button } from 'react-bootstrap';
import Helmet from 'react-helmet';
import ReactHtmlParser from 'react-html-parser';
import styled from 'styled-components';

import { Content, Header, Wrapper } from '../components/Article/styles/styles';
import { media } from '../config/theme';
import GlobalLayout from '../layouts';
import { BlogContentsKey } from './_types/BlogContentsKey';

const BackButton = styled.div`
  padding: 1rem 0;
  @media ${media.tablet} {
  }
`;

interface IProps {
  data: BlogContentsKey
}

const BlogTemplates: React.FC<IProps> = (props) => {
  const { edges } = props.data.allPrismicGuelichBlogPost;
  return (
    <GlobalLayout propsData={props}>
      {
        edges.map(({
          node: { data }
        }, key) => {
          return (
            <Fragment key={`${key}-${data!.title!.text}`} >
              <Helmet title={`Blog | ${data!.title!.text}`} />
              <Header banner={data!.image!.url} >
                <BackButton>
                  <Button href='/blog' >
                    Back To Blog
                  </Button>
                </BackButton>
                <p style={{ color: 'white' }} >Blog Page</p>
                <h2 style={{ color: 'white' }} >{data!.title!.text}</h2>
              </Header>
              <Wrapper>
                <Content>
                  {ReactHtmlParser(data!.excerpt!.html)}
                </Content>
              </Wrapper>
            </Fragment>
          )
        })
      }
    </GlobalLayout>
  )
}

export default BlogTemplates;

export const BlogTemplateContent = graphql`
  query BlogContentsKey($keytext: String!) {
  allPrismicGuelichBlogPost(filter: {data: {title: {text: {eq: $keytext}}}}) {
    edges {
      node {
        data {
          image {
            alt
            url
          }
          category
          date
          excerpt {
            html
          }
          timetoread
          title {
            text
          }
        }
      }
    }
  }
}

`;
