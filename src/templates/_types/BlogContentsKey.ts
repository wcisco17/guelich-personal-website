/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: BlogContentsKey
// ====================================================

export interface BlogContentsKey_allPrismicGuelichBlogPost_edges_node_data_image {
  __typename: "PrismicGuelichBlogPostDataImage";
  alt: string | null;
  url: string | null;
}

export interface BlogContentsKey_allPrismicGuelichBlogPost_edges_node_data_excerpt {
  __typename: "PrismicGuelichBlogPostDataExcerpt";
  html: string | null;
}

export interface BlogContentsKey_allPrismicGuelichBlogPost_edges_node_data_title {
  __typename: "PrismicGuelichBlogPostDataTitle";
  text: string | null;
}

export interface BlogContentsKey_allPrismicGuelichBlogPost_edges_node_data {
  __typename: "PrismicGuelichBlogPostData";
  image: BlogContentsKey_allPrismicGuelichBlogPost_edges_node_data_image | null;
  category: string | null;
  date: any | null;
  excerpt: BlogContentsKey_allPrismicGuelichBlogPost_edges_node_data_excerpt | null;
  timetoread: number | null;
  title: BlogContentsKey_allPrismicGuelichBlogPost_edges_node_data_title | null;
}

export interface BlogContentsKey_allPrismicGuelichBlogPost_edges_node {
  __typename: "PrismicGuelichBlogPost";
  data: BlogContentsKey_allPrismicGuelichBlogPost_edges_node_data | null;
}

export interface BlogContentsKey_allPrismicGuelichBlogPost_edges {
  __typename: "PrismicGuelichBlogPostEdge";
  node: BlogContentsKey_allPrismicGuelichBlogPost_edges_node;
}

export interface BlogContentsKey_allPrismicGuelichBlogPost {
  __typename: "PrismicGuelichBlogPostConnection";
  edges: BlogContentsKey_allPrismicGuelichBlogPost_edges[];
}

export interface BlogContentsKey {
  allPrismicGuelichBlogPost: BlogContentsKey_allPrismicGuelichBlogPost;
}

export interface BlogContentsKeyVariables {
  keytext: string;
}
