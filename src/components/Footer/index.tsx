import * as React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { FaFacebookF, FaLinkedinIn, FaTwitter } from "react-icons/fa";
import styled from "styled-components";
import { links } from "../../config/hocs";
import { media } from "../../config/theme";

const Column = styled.div`
  background-color: #2e3032;
  height: 60%;
  width: 100%;
  color: white;

  .column-content {
    margin: 2.5rem 6.5rem;

    @media ${media.phone} {
      margin: 2.5rem;
    }
  }

  .content-icons {
    display: flex;
    align-items: center;
    justify-content: flex-start;
    margin: 3rem 0;

    @media ${media.phone} {
      margin: 2rem 0;
      justify-content: center;
      margin-top: 3rem;
    }

    .icons {
      cursor: pointer;
      background-color: white;
      padding: 10px;
      border-radius: 50%;
      &:nth-child(2) {
        margin: 0 20px;
      }
    }
  }
`;

const Footer: React.FC = () => {
  return (
    <Column>
      <Container>
        <Row>
          <Col>
            <div className="column-content">
              <h4>Find me</h4>
              <p>
                c/o Bangkok University School of Entrepreneurship and Management
                BUSEM Kluaynamthai City Campus / 119 Rama 4 Road / Bangkok 10110
                Thailand
              </p>
              <br />
              <br />
              <h4>About this site</h4>
              <p>
                Ulrike Guelich “Finding the GEMS” Looking at Female
                Entrepreneurship in Thailand and Asean
              </p>

              <div className="content-icons">
                <div className="icons">
                  <FaLinkedinIn
                    onClick={() => window.open(links.ld)}
                    color="#2977c9"
                    size={43}
                  />
                </div>
                <div className="icons">
                  <FaFacebookF
                    onClick={() => window.open(links.fac)}
                    color="#3b5998"
                    size={43}
                  />
                </div>
                <div className="icons">
                  <FaTwitter
                    onClick={() => window.open(links.tweet)}
                    color="#1DA1F2"
                    size={43}
                  />
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </Column>
  );
};

export default Footer;
