import { navigate } from 'gatsby';
import * as React from "react";
import { Fragment } from "react";
import { Col, Container, Nav, Navbar, Row } from "react-bootstrap";
import styled from "styled-components";
import { Divider } from "../common/layouts";

interface INav {
  title: string | null;
}

const Title = styled.h3`
  a {
    text-decoration: none;
  }
`;

const Column = styled(Col)`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const NavContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Navigation: React.FC<INav> = ({ title }) => {
  return (
    <Fragment>
      <div style={{ textAlign: "center", paddingTop: "2rem" }}>
        <Title>
          <a href="/" className="text-dark">
            {title}
          </a>
        </Title>

        <NavContainer>
          <Navbar
            collapseOnSelect
            expand="lg"
            bg="white"
            className="justify-content-center"
            variant="light"
          >
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="justify-content-center">
                <Nav.Item>
                  <Nav.Link onClick={() => navigate('/')} className="text-dark">
                    Home
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link onClick={() => navigate('/about')} className="text-dark">
                    About
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link onClick={() => navigate('/practices')} className="text-dark">
                    Practices
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link onClick={() => navigate('/gem')} className="text-dark">
                    GEM
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link onClick={() => navigate('/blog')} className="text-dark">
                    Blog
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link onClick={() => navigate('/contact')} className="text-dark">
                    Contact
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </NavContainer>
      </div>
      <Divider width="100%" />
      <Container>
        <Row>
          <Column lg="12">
            <p>#Speaker</p>
            <p>#Author</p>
            <p>#Reseacher</p>
          </Column>
        </Row>
      </Container>
    </Fragment>
  );
};

export default Navigation;
