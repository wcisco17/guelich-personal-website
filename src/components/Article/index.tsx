import { Link } from 'gatsby';
import React from 'react';
import TextTruncate from 'react-text-truncate';
import styled from 'styled-components';

import { Subline } from '../common/layouts';

const Post = styled.article`
  display: flex;
  flex-direction: column;
  margin-top: 3.5rem;
  margin-bottom: 3.5rem;
`;

const Title = styled.h2`
  position: relative;
  text-shadow: 0 12px 30px rgba(0, 0, 0, 0.15);
  margin-bottom: 0.75rem;
`;

const Initiale = styled.span`
  position: absolute;
  font-size: 7rem;
  transform: translate(-50%, -50%);
  opacity: 0.08;
  user-select: none;
  z-index: -1;
`;

const Excerpt = styled.p`
  grid-column: -1 / 1;
  margin-top: 1rem;
  margin-bottom: 1rem;
`;

interface Props {
  title: any;
  date: any;
  excerpt: any;
  slug: any;
  timeToRead: number;
  category: any;
}

export class Article extends React.PureComponent<Props> {
  public render() {
    const { title, date, excerpt, slug, timeToRead, category } = this.props;
    const firstChar = title.charAt(0);
    const s = slug.toLocaleLowerCase()
      .split(' ')
      .join()
      .replace(/,/g, '-')
      .trimEnd();

    return (
      <Post>
        <Title>
          <Initiale>{firstChar}</Initiale>
          <Link to={`/blog/${s}`}>{title}</Link>
        </Title>
        <Subline>
          {date} &mdash; {timeToRead} Min Read &mdash; In{" "}
          {category}
        </Subline>
        <Excerpt>
          <TextTruncate
            line={3}
            element="span"
            truncateText="…"
            text={excerpt}
            textTruncateChild={null}
          />
        </Excerpt>
      </Post>
    );
  }
}
