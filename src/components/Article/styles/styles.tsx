import { darken, lighten } from "polished";
import rgba from "polished/lib/color/rgba";
import React from "react";
import styled from "styled-components";
import { colors, media } from "../../../config/theme";

const HeaderWrapper: any = styled.header`
  position: relative;
  border-radius: 10px;
  margin: 2rem 0;
  background: linear-gradient(
      -185deg,
      ${rgba(darken(0.1, colors.primary), 0.6)},
      ${rgba(lighten(0.1, colors.grey.dark), 0.8)}
    ),
    url(${(props: any) => props.banner}) no-repeat;
  background-size: cover;
  padding: 0rem 2rem 5rem;
  text-align: center;
  ::after {
    background: transparent url(/assets/mask.svg) no-repeat bottom left;
    background-size: 101%;
    bottom: -2px;
    content: "";
    display: block;
    height: 100%;
    left: 0;
    position: absolute;
    width: 100%;
  }
  @media ${media.tablet} {
    padding: 2rem 2rem 6rem;
  }
  @media ${media.phone} {
    padding: 1rem 0.5rem 7rem;
  }
`;

const Contents = styled.div`
  position: relative;
  z-index: 999;
  a {
    color: white;
    &:hover {
      opacity: 0.85;
      color: white;
    }
  }
`;

interface Props {
  children: any;
  banner?: string | any;
}

export class Header extends React.PureComponent<Props> {
  public render() {
    return (
      <HeaderWrapper banner={this.props.banner}>
        <Contents>{this.props.children}</Contents>
      </HeaderWrapper>
    );
  }
}

export const Wrapper: any = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 5rem;
  max-width: ${(props: any) => (props.fullWidth ? "100%" : "100rem")};
  padding: ${(props: any) => (props.fullWidth ? "0" : "0 6rem")};
  @media ${media.tablet} {
    padding: ${(props: any) => (props.fullWidth ? "0" : "0 3rem")};
  }
  @media ${media.phone} {
    padding: ${(props: any) => (props.fullWidth ? "0" : "0 1rem")};
  }
`;

export const Content = styled.div`
  box-shadow: 0 4px 120px rgba(0, 0, 0, 0.1);
  border-radius: 1rem;
  padding: 2rem 4rem;
  background-color: white;
  z-index: 9000;
  margin-top: -4rem;

  img {
    width: 100%;
    border-radius: 10px;
  }

  form {
    p {
      label,
      input {
        display: block;
      }
      input {
        min-width: 275px;
      }
      textarea {
        resize: vertical;
        min-height: 150px;
        width: 100%;
      }
    }
  }
  @media ${media.tablet} {
    padding: 3rem 3rem;
  }
  @media ${media.phone} {
    padding: 2rem 1.5rem;

    p {
      font-size: 0.7rem;
      line-height: 1.2rem;
    }
  }
`;
