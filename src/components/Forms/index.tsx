import axios from "axios";
import * as React from "react";
import { Button, Form } from "react-bootstrap";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { FORM_ID } from "../../config/hocs";

export const Forms = () => {
  const [serverState, setServerState] = React.useState({
    submitting: false,
    status: {}
  });

  const handleServerResponse = (ok: any, msg: any, form: any) => {
    setServerState({
      submitting: false,
      status: { ok, msg }
    });
    if (ok) {
      form.reset();
    }
  };

  const handleOnSubmit = (e: any) => {
    e.preventDefault();
    const form = e.target;
    setServerState({ submitting: true, status: {} });
    axios({
      method: "post",
      url: `https://getform.io/f/${FORM_ID}`,
      data: new FormData(form)
    })
      .then((r: any) => {
        handleServerResponse(true, "Thanks!", form);
        notify();
      })
      .catch(r => {
        handleServerResponse(false, r.response.data.error, form);
        notifyError();
      });
  };

  const notify = () => toast.success("Thank you we'll get back to you soon!");
  const notifyError = () => toast.error("Oops Sorry but there was an error");

  return (
    <Form onSubmit={handleOnSubmit} method="post">
      <ToastContainer />

      {serverState.status && (
        <p className={!(serverState as any).status.ok ? "errorMsg" : ""}>
          {(serverState as any).status.msg}
        </p>
      )}
      <Form.Group>
        <Form.Label>Name</Form.Label>
        <Form.Control
          required
          placeholder="Enter Your Name"
          type="text"
          id="name"
          name="name"
        />
      </Form.Group>

      <Form.Group>
        <Form.Label>Email address</Form.Label>
        <Form.Control
          required
          placeholder="Enter email"
          type="email"
          id="email"
          name="email"
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group>
        <Form.Label>Message</Form.Label>
        <Form.Control
          required
          as="textarea"
          rows="3"
          type="textarea"
          placeholder="Enter your message"
          id="textarea"
          name="textarea"
        />
      </Form.Group>

      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
  );
};
