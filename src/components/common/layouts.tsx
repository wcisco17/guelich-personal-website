import * as React from 'react';
import styled from 'styled-components';

// Global --- Divider
const Divide = styled.div`
  display: block;
  margin: 0 auto;
`;

interface IDivide {
  backgroundColor?: string;
  width: string;
}

export const Divider = ({ backgroundColor, width }: IDivide) => (
  <Divide style={{ width }} children={<hr style={{ backgroundColor }} />} />
);

// Global -- Subline

export const Subline = styled.div`
color: black;
text-align: left;
`;
