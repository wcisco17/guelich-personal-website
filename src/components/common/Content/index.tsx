import * as React from "react";
import { Col, Container, Row } from "react-bootstrap";
import styled from "styled-components";
import { media } from "../../../config/theme";

const Column = styled(Col)`
  .content-image {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }

  .content-text {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    margin: 3rem 6.5rem;
    text-align: left;

    @media ${media.phone} {
      align-items: center !important;
      text-align: center;
      margin: 2rem;
    }
  }
`;

const Image = styled.img`
  width: 100%;
`;

const Title = styled.h3``;

interface IContentProps {
  image: string;
  alt: string;

  title?: any;
  content?: any;
}

const Content: React.FC<IContentProps> = ({ image, title, alt, content }) => {
  return (
    <Container>
      <Row>
        <Column>
          <div className="content-image">
            <Image src={image} alt={alt} />
          </div>
        </Column>
      </Row>

      <Row>
        <Column>
          <div className="content-text">
            <Title>{title}</Title>
            <div className="content-text-main">{content}</div>
          </div>
        </Column>
      </Row>
    </Container>
  );
};

export default Content;
