import * as React from 'react';

import GlobalLayout from '../layouts';
import Blog from '../modules/Blog';

export default (props: any) => (
  <GlobalLayout propsData={props}>
    <Blog />
  </GlobalLayout>
);
