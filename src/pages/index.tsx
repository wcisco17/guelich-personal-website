import * as React from "react";
import GlobalLayout from "../layouts";
import HomePage from "../modules/Home";

export default (props: any) => (
  <GlobalLayout propsData={props}>
    <HomePage />
  </GlobalLayout>
);
