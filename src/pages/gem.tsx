import * as React from "react";
import GlobalLayout from "../layouts";
import Gem from "../modules/Gem";

export default (props: any) => (
  <GlobalLayout propsData={props}>
    <Gem />
  </GlobalLayout>
);
