import * as React from "react";
import GlobalLayout from "../layouts";
import AboutUs from "../modules/About";

export default (props: any) => (
  <GlobalLayout propsData={props}>
    <AboutUs />
  </GlobalLayout>
);
