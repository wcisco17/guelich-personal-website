import * as React from "react";
import GlobalLayout from "../layouts";
import Practices from "../modules/Practice";

export default (props: any) => (
  <GlobalLayout propsData={props}>
    <Practices />
  </GlobalLayout>
);
