import * as React from "react";
import GlobalLayout from "../layouts";
import ContactUs from "../modules/Contact";

export default (props: any) => (
  <GlobalLayout propsData={props}>
    <ContactUs />
  </GlobalLayout>
);
