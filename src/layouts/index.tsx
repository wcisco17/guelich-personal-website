import "bootstrap/dist/css/bootstrap.min.css";
import { graphql, useStaticQuery } from "gatsby";
import * as React from "react";
import { Container } from "react-bootstrap";
import ReactHelmet from "react-helmet";
import styled, { ThemeProvider } from "styled-components";
import Footer from "../components/Footer";
import Navigation from "../components/Navigation";
import { theme } from "../config/theme";
import { GlobalQuery } from "./_types/GlobalQuery";

interface IGlobalProps {
  propsData: any;
}

const GlobalContainer = styled.div``;

const GlobalLayout: React.FC<IGlobalProps> = props => {
  const data = useStaticQuery<GlobalQuery>(globalQuery);
  const { title } = data.site!.siteMetadata!;

  return (
    <ThemeProvider theme={theme}>
      <GlobalContainer>
        <ReactHelmet title={title as any} />
        <Container>
          <Navigation title={title} />
          {props.children}
        </Container>
        <Footer />
      </GlobalContainer>
    </ThemeProvider>
  );
};

export default GlobalLayout;

export const globalQuery = graphql`
  query GlobalQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`;
