/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GlobalQuery
// ====================================================

export interface GlobalQuery_site_siteMetadata {
  __typename: "SiteSiteMetadata";
  title: string | null;
}

export interface GlobalQuery_site {
  __typename: "Site";
  siteMetadata: GlobalQuery_site_siteMetadata | null;
}

export interface GlobalQuery {
  site: GlobalQuery_site | null;
}
