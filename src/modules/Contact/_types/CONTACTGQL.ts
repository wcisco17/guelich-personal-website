/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CONTACTGQL
// ====================================================

export interface CONTACTGQL_allPrismicGuelichContent_edges_node_data_contact_content {
  __typename: "PrismicGuelichContentDataContact_content";
  html: string | null;
  text: string | null;
}

export interface CONTACTGQL_allPrismicGuelichContent_edges_node_data_contact_image {
  __typename: "PrismicGuelichContentDataContact_image";
  alt: string | null;
  url: string | null;
}

export interface CONTACTGQL_allPrismicGuelichContent_edges_node_data_contact_title {
  __typename: "PrismicGuelichContentDataContact_title";
  text: string | null;
  html: string | null;
}

export interface CONTACTGQL_allPrismicGuelichContent_edges_node_data {
  __typename: "PrismicGuelichContentData";
  contact_content: CONTACTGQL_allPrismicGuelichContent_edges_node_data_contact_content | null;
  contact_image: CONTACTGQL_allPrismicGuelichContent_edges_node_data_contact_image | null;
  contact_title: CONTACTGQL_allPrismicGuelichContent_edges_node_data_contact_title | null;
}

export interface CONTACTGQL_allPrismicGuelichContent_edges_node {
  __typename: "PrismicGuelichContent";
  data: CONTACTGQL_allPrismicGuelichContent_edges_node_data | null;
}

export interface CONTACTGQL_allPrismicGuelichContent_edges {
  __typename: "PrismicGuelichContentEdge";
  node: CONTACTGQL_allPrismicGuelichContent_edges_node;
}

export interface CONTACTGQL_allPrismicGuelichContent {
  __typename: "PrismicGuelichContentConnection";
  edges: CONTACTGQL_allPrismicGuelichContent_edges[];
}

export interface CONTACTGQL {
  allPrismicGuelichContent: CONTACTGQL_allPrismicGuelichContent;
}
