import { graphql, useStaticQuery } from "gatsby";
import * as React from "react";
import { Fragment } from "react";
import { Container } from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";
import Content from "../../components/common/Content";
import { Forms } from "../../components/Forms";
import { CONTACTGQL } from "./_types/CONTACTGQL";

const ContactUs: React.FC = () => {
  const data = useStaticQuery<CONTACTGQL>(CONTACTCOGQL);
  const { edges } = data.allPrismicGuelichContent;

  return (
    <Container>
      {edges.map(({ node: { data } }, k) => {
        return (
          <Fragment key={k}>
            <Content
              image={data!.contact_image!.url as any}
              alt={data!.contact_image!.alt as any}
            />
            <div
              style={{ marginTop: "2rem", marginBottom: "5rem" }}
              className="container-forms"
            >
              <div className="text-center my-4">
                {ReactHtmlParser(data!.contact_title!.html)}
                {ReactHtmlParser(data!.contact_content!.html)}
              </div>
              <Forms />
            </div>
          </Fragment>
        );
      })}
    </Container>
  );
};

export default ContactUs;

export const CONTACTCOGQL = graphql`
  query CONTACTGQL {
    allPrismicGuelichContent {
      edges {
        node {
          data {
            contact_content {
              html
              text
            }
            contact_image {
              alt
              url
            }
            contact_title {
              text
              html
            }
          }
        }
      }
    }
  }
`;
