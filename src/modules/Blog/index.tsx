import { graphql, useStaticQuery } from "gatsby";
import * as React from "react";
import { Col, Container, Row } from "react-bootstrap";
import styled from "styled-components";
import { Article } from "../../components/Article";
import { AllBlogContent } from "./_types/AllBlogContent";

const Homepage = styled.main`
  display: flex;
  height: 100%;
  flex-direction: column;
`;

const Blog: React.FC = () => {
  const data = useStaticQuery<AllBlogContent>(BlogContent);
  const { edges } = data.allPrismicGuelichBlogPost;
  return (
    <>
      <h2 style={{ textAlign: "center" }}>Latest Stories</h2>
      <Container>
        <Row>
          <Col
            style={{
              marginLeft: "2rem",
              marginRight: "2rem"
            }}
          >
            <Homepage>
              {edges.map(({ node: { data } }, key) => {
                return (
                  <Article
                    key={key}
                    title={data!.title!.text}
                    date={data!.date}
                    excerpt={data!.excerpt?.text}
                    slug={data!.title!.text}
                    timeToRead={2}
                    category={data!.category}
                  />
                );
              })}
            </Homepage>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Blog;

export const BlogContent = graphql`
  query AllBlogContent {
    allPrismicGuelichBlogPost {
      edges {
        node {
          data {
            category
            date
            excerpt {
              text
            }
            timetoread
            title {
              text
            }
          }
        }
      }
    }
  }
`;
