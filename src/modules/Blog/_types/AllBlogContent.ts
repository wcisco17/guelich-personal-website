/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: AllBlogContent
// ====================================================

export interface AllBlogContent_allPrismicGuelichBlogPost_edges_node_data_excerpt {
  __typename: "PrismicGuelichBlogPostDataExcerpt";
  text: string | null;
}

export interface AllBlogContent_allPrismicGuelichBlogPost_edges_node_data_title {
  __typename: "PrismicGuelichBlogPostDataTitle";
  text: string | null;
}

export interface AllBlogContent_allPrismicGuelichBlogPost_edges_node_data {
  __typename: "PrismicGuelichBlogPostData";
  category: string | null;
  date: any | null;
  excerpt: AllBlogContent_allPrismicGuelichBlogPost_edges_node_data_excerpt | null;
  timetoread: number | null;
  title: AllBlogContent_allPrismicGuelichBlogPost_edges_node_data_title | null;
}

export interface AllBlogContent_allPrismicGuelichBlogPost_edges_node {
  __typename: "PrismicGuelichBlogPost";
  data: AllBlogContent_allPrismicGuelichBlogPost_edges_node_data | null;
}

export interface AllBlogContent_allPrismicGuelichBlogPost_edges {
  __typename: "PrismicGuelichBlogPostEdge";
  node: AllBlogContent_allPrismicGuelichBlogPost_edges_node;
}

export interface AllBlogContent_allPrismicGuelichBlogPost {
  __typename: "PrismicGuelichBlogPostConnection";
  edges: AllBlogContent_allPrismicGuelichBlogPost_edges[];
}

export interface AllBlogContent {
  allPrismicGuelichBlogPost: AllBlogContent_allPrismicGuelichBlogPost;
}
