import { graphql, useStaticQuery } from "gatsby";
import * as React from "react";
import { Fragment } from "react";
import { Container } from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";
import Content from "../../components/common/Content";
import { GEMHQL } from "./_types/GEMHQL";

const Gem: React.FC = () => {
  const data = useStaticQuery<GEMHQL>(GEMQLS);
  const { edges } = data.allPrismicGuelichContent;

  return (
    <Container>
      {edges.map(({ node: { data } }, k) => {
        return (
          <Fragment key={k}>
            <Content
              image={data!.gem_image!.url as any}
              alt={data!.gem_image!.alt as any}
              title={data!.gem_title!.text}
              content={ReactHtmlParser(data!.gem_content!.html)}
            />
          </Fragment>
        );
      })}
    </Container>
  );
};

export default Gem;

export const GEMQLS = graphql`
  query GEMHQL {
    allPrismicGuelichContent {
      edges {
        node {
          data {
            gem_content {
              html
            }
            gem_image {
              alt
              url
            }
            gem_title {
              text
            }
          }
        }
      }
    }
  }
`;
