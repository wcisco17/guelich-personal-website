/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GEMHQL
// ====================================================

export interface GEMHQL_allPrismicGuelichContent_edges_node_data_gem_content {
  __typename: "PrismicGuelichContentDataGem_content";
  html: string | null;
}

export interface GEMHQL_allPrismicGuelichContent_edges_node_data_gem_image {
  __typename: "PrismicGuelichContentDataGem_image";
  alt: string | null;
  url: string | null;
}

export interface GEMHQL_allPrismicGuelichContent_edges_node_data_gem_title {
  __typename: "PrismicGuelichContentDataGem_title";
  text: string | null;
}

export interface GEMHQL_allPrismicGuelichContent_edges_node_data {
  __typename: "PrismicGuelichContentData";
  gem_content: GEMHQL_allPrismicGuelichContent_edges_node_data_gem_content | null;
  gem_image: GEMHQL_allPrismicGuelichContent_edges_node_data_gem_image | null;
  gem_title: GEMHQL_allPrismicGuelichContent_edges_node_data_gem_title | null;
}

export interface GEMHQL_allPrismicGuelichContent_edges_node {
  __typename: "PrismicGuelichContent";
  data: GEMHQL_allPrismicGuelichContent_edges_node_data | null;
}

export interface GEMHQL_allPrismicGuelichContent_edges {
  __typename: "PrismicGuelichContentEdge";
  node: GEMHQL_allPrismicGuelichContent_edges_node;
}

export interface GEMHQL_allPrismicGuelichContent {
  __typename: "PrismicGuelichContentConnection";
  edges: GEMHQL_allPrismicGuelichContent_edges[];
}

export interface GEMHQL {
  allPrismicGuelichContent: GEMHQL_allPrismicGuelichContent;
}
