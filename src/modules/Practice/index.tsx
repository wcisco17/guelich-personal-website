import { graphql, useStaticQuery } from "gatsby";
import * as React from "react";
import { Fragment } from "react";
import { Container } from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";
import Content from "../../components/common/Content";
import { PRACTICES } from "./_types/PRACTICES";

const Practices: React.FC = () => {
  const data = useStaticQuery<PRACTICES>(PRACTICESGQL);
  const { edges } = data.allPrismicGuelichContent;

  return (
    <Container>
      {edges.map(({ node: { data } }, k) => {
        return (
          <Fragment key={k}>
            <Content
              image={data!.practices_image!.url as any}
              alt={data!.practices_image!.alt as any}
              title={data!.practices_title!.text}
              content={ReactHtmlParser(data!.practices_content!.html)}
            />
          </Fragment>
        );
      })}
    </Container>
  );
};

export default Practices;

export const PRACTICESGQL = graphql`
  query PRACTICES {
    allPrismicGuelichContent {
      edges {
        node {
          data {
            practices_content {
              html
            }
            practices_image {
              alt
              url
            }
            practices_title {
              text
            }
          }
        }
      }
    }
  }
`;
