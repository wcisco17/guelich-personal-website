/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PRACTICES
// ====================================================

export interface PRACTICES_allPrismicGuelichContent_edges_node_data_practices_content {
  __typename: "PrismicGuelichContentDataPractices_content";
  html: string | null;
}

export interface PRACTICES_allPrismicGuelichContent_edges_node_data_practices_image {
  __typename: "PrismicGuelichContentDataPractices_image";
  alt: string | null;
  url: string | null;
}

export interface PRACTICES_allPrismicGuelichContent_edges_node_data_practices_title {
  __typename: "PrismicGuelichContentDataPractices_title";
  text: string | null;
}

export interface PRACTICES_allPrismicGuelichContent_edges_node_data {
  __typename: "PrismicGuelichContentData";
  practices_content: PRACTICES_allPrismicGuelichContent_edges_node_data_practices_content | null;
  practices_image: PRACTICES_allPrismicGuelichContent_edges_node_data_practices_image | null;
  practices_title: PRACTICES_allPrismicGuelichContent_edges_node_data_practices_title | null;
}

export interface PRACTICES_allPrismicGuelichContent_edges_node {
  __typename: "PrismicGuelichContent";
  data: PRACTICES_allPrismicGuelichContent_edges_node_data | null;
}

export interface PRACTICES_allPrismicGuelichContent_edges {
  __typename: "PrismicGuelichContentEdge";
  node: PRACTICES_allPrismicGuelichContent_edges_node;
}

export interface PRACTICES_allPrismicGuelichContent {
  __typename: "PrismicGuelichContentConnection";
  edges: PRACTICES_allPrismicGuelichContent_edges[];
}

export interface PRACTICES {
  allPrismicGuelichContent: PRACTICES_allPrismicGuelichContent;
}
