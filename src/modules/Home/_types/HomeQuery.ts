/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: HomeQuery
// ====================================================

export interface HomeQuery_allPrismicGuelichContent_edges_node_data_gue_image {
  __typename: "PrismicGuelichContentDataGue_image";
  alt: string | null;
  url: string | null;
}

export interface HomeQuery_allPrismicGuelichContent_edges_node_data_maintitle {
  __typename: "PrismicGuelichContentDataMaintitle";
  text: string | null;
}

export interface HomeQuery_allPrismicGuelichContent_edges_node_data_content {
  __typename: "PrismicGuelichContentDataContent";
  html: string | null;
}

export interface HomeQuery_allPrismicGuelichContent_edges_node_data {
  __typename: "PrismicGuelichContentData";
  gue_image: HomeQuery_allPrismicGuelichContent_edges_node_data_gue_image | null;
  maintitle: HomeQuery_allPrismicGuelichContent_edges_node_data_maintitle | null;
  content: HomeQuery_allPrismicGuelichContent_edges_node_data_content | null;
}

export interface HomeQuery_allPrismicGuelichContent_edges_node {
  __typename: "PrismicGuelichContent";
  data: HomeQuery_allPrismicGuelichContent_edges_node_data | null;
}

export interface HomeQuery_allPrismicGuelichContent_edges {
  __typename: "PrismicGuelichContentEdge";
  node: HomeQuery_allPrismicGuelichContent_edges_node;
}

export interface HomeQuery_allPrismicGuelichContent {
  __typename: "PrismicGuelichContentConnection";
  edges: HomeQuery_allPrismicGuelichContent_edges[];
}

export interface HomeQuery {
  allPrismicGuelichContent: HomeQuery_allPrismicGuelichContent;
}
