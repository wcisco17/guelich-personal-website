import { graphql, useStaticQuery } from "gatsby";
import * as React from "react";
import { Fragment } from "react";
import { Container } from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";
import Content from "../../components/common/Content";
import { HomeQuery } from "./_types/HomeQuery";

const HomePage: React.FC = () => {
  const data = useStaticQuery<HomeQuery>(HomePageGQL);
  const { edges } = data.allPrismicGuelichContent;

  return (
    <Container>
      {edges.map(({ node: { data } }, k) => {
        return (
          <Fragment key={k}>
            <Content
              image={data!.gue_image!.url as any}
              alt={data!.gue_image!.alt as any}
              title={data!.maintitle!.text}
              content={ReactHtmlParser(data!.content!.html)}
            />
          </Fragment>
        );
      })}
    </Container>
  );
};

export default HomePage;

export const HomePageGQL = graphql`
  query HomeQuery {
    allPrismicGuelichContent {
      edges {
        node {
          data {
            gue_image {
              alt
              url
            }
            maintitle {
              text
            }
            content {
              html
            }
          }
        }
      }
    }
  }
`;
