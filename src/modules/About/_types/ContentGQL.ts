/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContentGQL
// ====================================================

export interface ContentGQL_allPrismicGuelichContent_edges_node_data_about_title {
  __typename: "PrismicGuelichContentDataAbout_title";
  text: string | null;
}

export interface ContentGQL_allPrismicGuelichContent_edges_node_data_about_image {
  __typename: "PrismicGuelichContentDataAbout_image";
  url: string | null;
  alt: string | null;
}

export interface ContentGQL_allPrismicGuelichContent_edges_node_data_about_content {
  __typename: "PrismicGuelichContentDataAbout_content";
  html: string | null;
}

export interface ContentGQL_allPrismicGuelichContent_edges_node_data {
  __typename: "PrismicGuelichContentData";
  about_title: ContentGQL_allPrismicGuelichContent_edges_node_data_about_title | null;
  about_image: ContentGQL_allPrismicGuelichContent_edges_node_data_about_image | null;
  about_content: ContentGQL_allPrismicGuelichContent_edges_node_data_about_content | null;
}

export interface ContentGQL_allPrismicGuelichContent_edges_node {
  __typename: "PrismicGuelichContent";
  data: ContentGQL_allPrismicGuelichContent_edges_node_data | null;
}

export interface ContentGQL_allPrismicGuelichContent_edges {
  __typename: "PrismicGuelichContentEdge";
  node: ContentGQL_allPrismicGuelichContent_edges_node;
}

export interface ContentGQL_allPrismicGuelichContent {
  __typename: "PrismicGuelichContentConnection";
  edges: ContentGQL_allPrismicGuelichContent_edges[];
}

export interface ContentGQL {
  allPrismicGuelichContent: ContentGQL_allPrismicGuelichContent;
}
