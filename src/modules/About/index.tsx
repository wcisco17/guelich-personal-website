import { graphql, useStaticQuery } from "gatsby";
import * as React from "react";
import { Fragment } from "react";
import { Container } from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";
import Content from "../../components/common/Content";
import { ContentGQL } from "./_types/ContentGQL";

const AboutUs: React.FC = () => {
  const data = useStaticQuery<ContentGQL>(ContentGQLS);
  const { edges } = data.allPrismicGuelichContent;

  return (
    <Container>
      {edges.map(({ node: { data } }, k) => {
        return (
          <Fragment key={k}>
            <Content
              image={data!.about_image!.url as any}
              alt={data!.about_image!.alt as any}
              title={data!.about_title!.text}
              content={ReactHtmlParser(data!.about_content!.html)}
            />
          </Fragment>
        );
      })}
    </Container>
  );
};

export default AboutUs;

export const ContentGQLS = graphql`
  query ContentGQL {
    allPrismicGuelichContent {
      edges {
        node {
          data {
            about_title {
              text
            }
            about_image {
              url
              alt
            }
            about_content {
              html
            }
          }
        }
      }
    }
  }
`;
