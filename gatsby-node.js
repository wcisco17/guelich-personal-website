const _ = require("lodash");

exports.createPages = async ({ actions, graphql }) => {
  const blogPost = await graphql(`
    {
      allPrismicGuelichBlogPost {
        edges {
          node {
            data {
              title {
                text
              }
            }
          }
        }
      }
    }
  `);

  return blogPost.data.allPrismicGuelichBlogPost.edges.forEach(postText => {
    const link = postText.node.data.title.text
      .toLocaleLowerCase()
      .split(" ")
      .join()
      .replace(/,/g, "-")
      .trimEnd();

    return actions.createPage({
      path: `/blog/${link}`,
      component: require.resolve(`./src/templates/blog.tsx`),
      context: { keytext: postText.node.data.title.text }
    });
  });
};
